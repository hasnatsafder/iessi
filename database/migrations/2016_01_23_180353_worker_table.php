<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ss_number')->nullable();
            $table->string('name');
            $table->string('father_name');
            $table->string('cnic');
            $table->string('mobile_number');
            $table->string('address');
            $table->string('gender');
            $table->string('martial_status');
            $table->string('profile_picture')->nullable();
            $table->boolean('blocked')->default(0); 
            $table->integer('company_id')->unsigned()->nullable();
            $table->integer('dispensary_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('worker');
    }
}
