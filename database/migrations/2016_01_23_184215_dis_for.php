<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DisFor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dependent', function (Blueprint $table) {
                        $table->foreign('dispensary_id')
                ->references('id')
                ->on('dispensary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dependent', function (Blueprint $table) {
            //
        });
    }
}
