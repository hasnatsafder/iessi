<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedicalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_billing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dispensary_id')->unsigned()->nullable();
            $table->integer('patient_id');
            $table->string('patient_type');
            $table->integer('medicine_id')->unsigned()->nullable();
            $table->integer('medicine_quantity');
            $table->integer('medicine_price');
            $table->integer('lab_id')->unsigned()->nullable();
            $table->integer('lab_quantity');
            $table->integer('lab_price');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_billing');
    }
}
