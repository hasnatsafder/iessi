<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DepandantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ss_number');
            $table->string('name');
            $table->string('father_name');
            $table->string('cnic');
            $table->string('marital_status');
            $table->string('relations');
            $table->boolean('blocked');
            $table->string('profile_picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dependent');
    }
}
