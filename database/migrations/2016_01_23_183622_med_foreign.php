<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_billing', function (Blueprint $table) {
                $table->foreign('dispensary_id')
                ->references('id')
                ->on('dispensary');

            $table->foreign('medicine_id')
                ->references('id')
                ->on('medicine_registeration'); 
                
            $table->foreign('lab_id')
                ->references('id')
                ->on('lab_test');               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_billing', function (Blueprint $table) {
            //
        });
    }
}
