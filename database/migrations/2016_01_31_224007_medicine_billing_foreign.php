<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedicineBillingForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicine_medical_billing', function (Blueprint $table) {
            $table->foreign('medicine_id')
                ->references('id')
                ->on('medicines');

            $table->foreign('medical_billing_id')
                ->references('id')
                ->on('medical_billing');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicine_medical_billing', function (Blueprint $table) {
            //
        });
    }
}
