<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contribution', function (Blueprint $table) {
            $table->foreign('company_id')
            ->references('id')
            ->on('company');

            $table->foreign('worker_id')
                ->references('id')
                ->on('worker');                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contribution', function (Blueprint $table) {
            //
        });
    }
}
