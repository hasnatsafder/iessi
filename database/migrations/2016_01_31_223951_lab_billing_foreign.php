<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LabBillingForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lab_medical_billing', function (Blueprint $table) {
            $table->foreign('lab_id')
                ->references('id')
                ->on('lab_tests');
            $table->foreign('medical_billing_id')
                ->references('id')
                ->on('medical_billing');                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lab_medical_billing', function (Blueprint $table) {
            //
        });
    }
}
