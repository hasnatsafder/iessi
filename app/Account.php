<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['cheque_number', 'amount', 'status', 'date_cheque', 'date_deposit'];
}
