<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateAdmin
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */

    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user) {

            if($user->service_type == 6)
            {
                return $next($request);
            }
            else
            {
                return redirect('/');
            }

        }
        else{
            return redirect('/');
        }

    }
}
