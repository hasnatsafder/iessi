<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contribution;
use DB;
use App\Company;
use App\Worker;

class ContributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $company = $request->input('company');
        $month = $request->input('month');

        $workers = Worker::where('company_id', $company)->get();

        foreach ($workers as $key => $worker) {
            $contribution = new Contribution;
            $contribution->company_id = $worker->company_id;
            $contribution->worker_id = $worker->id;
            $contribution->billing_month = $month;
            $contribution->save();
        }

        return redirect ("contribution/show/$company/$month");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company, $month)
    {

        $errorBit = false;

        $arr = explode('-', $month);
        if ($arr[0] > 12 ) {
            $errorBit = true;
        }
        if ($arr[1] < 2013 || $arr[1] > 2200) {
            $errorBit = true;
        }


        if ($errorBit) {
              // return redirect('contribution/show/' . $company . '/' . $month)
              //   ->withErrors(['message' => 'Enter Correct Dates'])
              //   ->withInput();
            return Redirect::back()->withErrors(['Enter Correct Dates']);
        }

        $companyWorkers = Worker::where('company_id', $company)->get();

        foreach ($companyWorkers as $key => $value) {

        $contributions = DB::table('contributions')
                            ->join('workers', 'contributions.worker_id', '=', 'workers.id')
                            ->where(['contributions.worker_id' => $value->id, 'contributions.billing_month' => $month])
                            ->select('contributions.id as id',
                            'contributions.contribution as contribution',
                            'workers.ss_number as ss_number',
                            'workers.name as name',
                            'workers.father_name as father_name',
                            'workers.cnic as cnic')
                            ->get();

            if (!$contributions)
            {
                $contribution = new Contribution;
                $contribution->company_id = $value->company_id;
                $contribution->worker_id = $value->id;
                $contribution->billing_month = $month;
                $contribution->save();
            }

        }

        $contributions = DB::table('contributions')
                            ->join('workers', 'contributions.worker_id', '=', 'workers.id')
                            ->where(['contributions.company_id' => $company, 'contributions.billing_month' => $month])
                            ->select('contributions.id as id',
                            'contributions.contribution as contribution',
                            'workers.ss_number as ss_number',
                            'workers.name as name',
                            'workers.father_name as father_name',
                            'workers.cnic as cnic')
                            ->get();  


        $companies_all = Company::orderBy('name', 'asc')->get();

        foreach($companies_all as $company_all)
        {
            $companies [$company_all->id] = $company_all->name;
        }   

        return View('dashboard.contribution.show', compact('contributions', 'companies', 'company', 'month'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, $value)
    {
        $contribution = Contribution::find($id);
        $contribution->contribution = $value;
        $contribution->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
