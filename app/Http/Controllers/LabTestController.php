<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LabTest;
use Validator;

class LabTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lab_tests = LabTest::all();

        return View('dashboard.lab.index', compact('lab_tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view ('dashboard.lab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'price' => 'numeric|required' , 
            'name' => 'required|max:20'
        ]);
        // Check to see if validation fails or passes
        if ($validator->fails())
        {
            return redirect('lab/create')->withInput()->withErrors($validator);   
        }

        LabTest::create($request->input());

        return redirect('lab');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lab = LabTest::find($id);

        return View('dashboard.lab.edit', compact('lab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'price' => 'numeric|required' , 
            'name' => 'required|max:20'
        ]);
        // Check to see if validation fails or passes
        if ($validator->fails())
        {
            return redirect('lab/'.$id.'/edit')->withInput()->withErrors($validator);   
        }

        $lab = LabTest::find($id);
        $lab->update($request->input());

        return redirect('lab');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lab = LabTest::find($id);

        $lab->delete($lab);

        return redirect('lab');
    }
}
