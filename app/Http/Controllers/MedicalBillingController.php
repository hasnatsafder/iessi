<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Dispensary;
use DB;
use App\MedicalBilling;
use App\Medicine;
use App\LabTest;
use App\MedicineMedicalBilling;
use App\LabMedicalBilling;

class MedicalBillingController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $total = [];

        $medical_billings = MedicalBilling::all();

        $dispensary = [];

        foreach ($medical_billings as $key => $medical_billing) {
            $disc = Dispensary::find($medical_billing->dispensary_id);
            $dispensary[$key] = $disc->name;

            $total[$key] = 0;

            //getting value of medicines 
            $medicines = MedicineMedicalBilling::where('medical_billing_id', $medical_billing->id)->get();
            foreach ($medicines as $key1 => $value) {
                $med = Medicine::find($value->medicine_id);
                $total[$key] = $total[$key] + $med->price * $value->quantity;
            }

            //getting value of lab 
            $labs = LabMedicalBilling::where('medical_billing_id', $medical_billing->id)->get();
            foreach ($labs as $key2 => $value) {
                $lab = LabTest::find($value->lab_id);
                $total[$key] = $total[$key] + $lab->price;
            }

        }

        return View('dashboard.medical_billing.index', compact('medical_billings', 'dispensary', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dispensaries_all = Dispensary::all();

        $dispensaries = [];

        foreach ($dispensaries_all as $key => $value) {
            $dispensaries[$value->id] = $value->name;
        }

        return View('dashboard.medical_billing.create', compact('dispensaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $medical_billing  = new MedicalBilling;

        $medical_billing->dispensary_id = $request->input('dispensary_id');
        $medical_billing->patient_id = $request->input('patient_id');

        $medical_billing->save();

        return redirect('medical_billing');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medicines_all = Medicine::all();

        $medicines = [];

        foreach ($medicines_all as $key => $value) {
            $medicines[$value->id] = $value->name;
        }

        $lab_tests_all = LabTest::all();

        $lab_tests = [];

        foreach ($lab_tests_all as $key => $value) {
            $lab_tests[$value->id] = $value->name;
        }

        //get medicines for that medical billing
        $medicines_medical_billing = DB::table('medicine_medical_billings as mmb')
                                        ->join('medicines as m', 'm.id', '=', 'mmb.medicine_id')
                                        ->select('m.name as medicine', 
                                            'mmb.quantity as quantity')
                                        ->where('medical_billing_id', $id)
                                        ->get();

        //get lab for that medical billing
        $lab_medical_billing = DB::table('lab_medical_billings as lmb')
                                        ->join('lab_tests as l', 'l.id', '=', 'lmb.lab_id')
                                        ->select('l.name as lab')
                                        ->where('medical_billing_id', $id)
                                        ->get();                                        

        return View('dashboard.medical_billing.edit', compact('medicines', 'lab_tests', 'id', 'medicines_medical_billing', 'lab_medical_billing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /*Add Medicines*/

    public function add_medicine($medical_billing, $medicineval, $quantity)
    {

        $medicine = new MedicineMedicalBilling;

        $medicine->medical_billing_id = $medical_billing;
        $medicine->medicine_id = $medicineval;
        $medicine->quantity = $quantity;

        $medicine->save();

        return "Successfully Added";
 
    }

    /*Add Lab*/

    public function add_lab($medical_billing, $lab)
    {
        $lab_test = new LabMedicalBilling;

        $lab_test->medical_billing_id = $medical_billing;
        $lab_test->lab_id = $lab;
        $lab_test->save();

        return "Successfully Added";
 
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
