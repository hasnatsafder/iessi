<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Worker;
use App\Dependent;
use App\Dispensary;
use Input;
use Validator;
use DB;
use App\Company;

class DependantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dependents = DB::table('dependents')
                    ->join('workers' , 'workers.id' ,'=', 'dependents.worker_id')
                    ->join('dispensaries' , 'dependents.dispensary_id' ,'=', 'dispensaries.id')
                    ->select('dependents.id as id',
                    'dependents.name as name',
                    'dependents.ss_number as ss_number',
                    'dependents.father_name as father_name',
                    'dependents.cnic as cnic',
                    'dependents.blocked as blocked',
                    'dependents.relation as relation',
                    'dispensaries.name as dispensary_name',
                    'workers.name as worker_name')
                    ->get();

        return View('dashboard.dependant.index', compact('dependents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $worker_all = Worker::orderBy('name', 'asc')->get();

        foreach($worker_all as $worker_all)
        {
            $workers [$worker_all->id] = $worker_all->name . " - CNIC - ". $worker_all->cnic;
        }         

        $dispensaries_all = Dispensary::orderBy('name', 'asc')->get();

        foreach($dispensaries_all as $dispensary_all)
        {
            $dispensaries [$dispensary_all->id] = $dispensary_all->name;
        }    

        return View('dashboard.dependant.create', compact('workers', 'dispensaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'cnic' => 'numeric|required' , 
                'relation' => 'required' ,
                'name' => 'required|max:30' , 
                'father_name' => 'required' 
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('dependent/create')->withInput()->withErrors($validator);   
            }


        if ($request->input('ss_number'))
        {
            // Saving the documents
            //Validate Inputs
            $validator = Validator::make($request->all(),[
                'profile_picture_file' => 'mimes:jpg,png,pdf,doc,docx,jpeg|max:5120' , 
                'ss_number' => 'numeric|unique:workers|unique:dependents'
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('dependent/create')->withInput()->withErrors($validator);   
            }
            //saving file to folder
            $file = $request->input('profile_picture_file');
            $fileNameOriginal = pathinfo($file, PATHINFO_FILENAME); // file
            $extension = pathinfo($file, PATHINFO_EXTENSION); // jpg    
            $destinationPath = public_path().'/documents/';
            $extension = Input::file('profile_picture_file')->getClientOriginalExtension(); // getting image extension
            $fileName =  $request->input('ss_number').'.'.$extension; // renameing image
            if (Input::file('profile_picture_file')) 
            {
                 Input::file('profile_picture_file')->move($destinationPath, $fileName);
            }
            else
            {
                echo "no file";
            }

        }

        $dependent = new Dependent;
        $dependent->name = $request->input('name');
        $dependent->father_name = $request->input('father_name');
        $dependent->cnic = $request->input('cnic');
        $dependent->relation = $request->input('relation');
        $dependent->marital_status = $request->input('marital_status');
        $dependent->blocked = 0;
        $dependent->worker_id = $request->input('worker_id');
        $dependent->dispensary_id = $request->input('dispensary_id');
        $dependent->save();


        if ($request->input('ss_number'))
        {
            $dependent->ss_number = $request->input('ss_number');
            $dependent->profile_picture = $request->input('ss_number').'.'.$extension;
        }
        $dependent->save();
        return redirect('dependent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dependent = Dependent::find($id);

        $worker_all = Worker::orderBy('name', 'asc')->get();

        foreach($worker_all as $worker_all)
        {
            $workers [$worker_all->id] = $worker_all->name . " - CNIC - ". $worker_all->cnic;
        }  

        $dispensaries_all = Dispensary::orderBy('name', 'asc')->get();

        foreach($dispensaries_all as $dispensary_all)
        {
            $dispensaries [$dispensary_all->id] = $dispensary_all->name;
        }    

        return View('dashboard.dependant.edit', compact('dependent', 'workers', 'dispensaries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
                'cnic' => 'numeric|required' , 
                'relation' => 'required' ,
                'name' => 'required|max:30' , 
                'father_name' => 'required' 
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('dependent/create')->withInput()->withErrors($validator);   
            }


        if ($request->input('ss_number'))
        {
            // Saving the documents
            //Validate Inputs
            $validator = Validator::make($request->all(),[
                'profile_picture_file' => 'mimes:jpg,png,pdf,doc,docx,jpeg|max:5120' , 
                'ss_number' => 'numeric|unique:workers|unique:dependents,ss_number,'.$id
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('dependent/create')->withInput()->withErrors($validator);   
            }
            //saving file to folder
            $file = $request->input('profile_picture_file');
            $fileNameOriginal = pathinfo($file, PATHINFO_FILENAME); // file
            $extension = pathinfo($file, PATHINFO_EXTENSION); // jpg    
            $destinationPath = public_path().'/documents/';
            $extension = Input::file('profile_picture_file')->getClientOriginalExtension(); // getting image extension
            $fileName =  $request->input('ss_number').'.'.$extension; // renameing image
            if (Input::file('profile_picture_file')) 
            {
                 Input::file('profile_picture_file')->move($destinationPath, $fileName);
            }
            else
            {
                echo "no file";
            }

        }

        $dependent = Dependent::find($id);
        $dependent->name = $request->input('name');
        $dependent->father_name = $request->input('father_name');
        $dependent->cnic = $request->input('cnic');
        $dependent->relation = $request->input('relation');
        $dependent->marital_status = $request->input('marital_status');
        $dependent->blocked = 0;
        $dependent->worker_id = $request->input('worker_id');
        $dependent->dispensary_id = $request->input('dispensary_id');
        $dependent->save();


        if ($request->input('ss_number'))
        {
            $dependent->ss_number = $request->input('ss_number');
            $dependent->profile_picture = $request->input('ss_number').'.'.$extension;
        }
        $dependent->save();
        return redirect('dependent');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // To delete a profile picture

    public function remove_profile_image($file_name)
    {
        File::delete(public_path().'/documents/'.$file_name);
        $dependent= Dependent::where('profile_picture', $file_name)->first();
        $dependent->profile_picture = '';
        $dependent->save();
    }
}
