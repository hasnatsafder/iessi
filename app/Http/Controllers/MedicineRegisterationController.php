<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Medicine;
use Validator;

class MedicineRegisterationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $medicines = Medicine::all();

        return View('dashboard.medicine.index', compact('medicines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.medicine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'price' => 'numeric|required' , 
            'name' => 'required|max:20'
        ]);
        // Check to see if validation fails or passes
        if ($validator->fails())
        {
            return redirect('medicine/create')->withInput()->withErrors($validator);   
        }

        Medicine::create($request->input());

        return redirect('medicine');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $medicine = Medicine::find($id);

        return View('dashboard.medicine.edit', compact('medicine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'price' => 'numeric|required' , 
            'name' => 'required|max:20'
        ]);
        // Check to see if validation fails or passes
        if ($validator->fails())
        {
            return redirect('medicine/'.$id.'/edit')->withInput()->withErrors($validator);   
        }

        $medicine = Medicine::find($id);
        $medicine->update($request->input());

        return redirect('medicine');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $medicine = Medicine::find($id);

        $medicine->delete($medicine);

        return redirect('medicine');
    }
}
