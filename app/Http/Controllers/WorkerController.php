<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Worker;
use App\Company;
use App\Dispensary;
use Input;
use Validator;
use DB;
use File;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = DB::table('workers')
                    ->join('companies' , 'workers.company_id' ,'=', 'companies.id')
                    ->join('dispensaries' , 'workers.dispensary_id' ,'=', 'dispensaries.id')
                    ->select('workers.id as id',
                    'workers.name as name',
                    'workers.ss_number as ss_number',
                    'workers.father_name as father_name',
                    'workers.cnic as cnic',
                    'workers.blocked as blocked',
                    'companies.name as company_name',
                    'dispensaries.name as dispensary_name')
                    ->paginate(30);

        return View('dashboard.worker.index', compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $companies_all = Company::orderBy('name', 'asc')->get();

        foreach($companies_all as $company_all)
        {
            $companies [$company_all->id] = $company_all->name;
        }         

        $dispensaries_all = Dispensary::orderBy('name', 'asc')->get();

        foreach($dispensaries_all as $dispensary_all)
        {
            $dispensaries [$dispensary_all->id] = $dispensary_all->name;
        }    

        return View('dashboard.worker.create', compact('companies', 'dispensaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $validator = Validator::make($request->all(),[
                'cnic' => 'numeric|required' , 
                'mobile_number' => 'numeric|required' ,
                'name' => 'required|max:30' , 
                'father_name' => 'required' ,
                'address'  => 'required'
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('worker/create')->withInput()->withErrors($validator);   
            }


        if ($request->input('ss_number'))
        {
            // Saving the documents
            //Validate Inputs
            $validator = Validator::make($request->all(),[
                'profile_picture_file' => 'required|mimes:jpg,png,pdf,doc,docx,jpeg|max:5120' , 
                'ss_number' => 'numeric|unique:workers|unique:dependents'
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('worker/create')->withInput()->withErrors($validator);   
            }
            //saving file to folder
            $file = $request->input('profile_picture_file');
            $fileNameOriginal = pathinfo($file, PATHINFO_FILENAME); // file
            $extension = pathinfo($file, PATHINFO_EXTENSION); // jpg    
            $destinationPath = public_path().'/documents/';
            $extension = Input::file('profile_picture_file')->getClientOriginalExtension(); // getting image extension
            $fileName =  $request->input('ss_number').'.'.$extension; // renameing image
            if (Input::file('profile_picture_file')) 
            {
                 Input::file('profile_picture_file')->move($destinationPath, $fileName);
            }
            else
            {
                echo "no file";
            }

        }

        $worker = new Worker;
        $worker->name = $request->input('name');
        $worker->father_name = $request->input('father_name');
        $worker->cnic = $request->input('cnic');
        $worker->mobile_number = $request->input('mobile_number');
        $worker->gender = $request->input('gender');
        $worker->address = $request->input('address');
        $worker->marital_status = $request->input('marital_status');
        $worker->blocked = 0;
        $worker->company_id = $request->input('company_id');
        $worker->dispensary_id = $request->input('dispensary_id');
        $worker->save();


        if ($request->input('ss_number'))
        {
            $worker->ss_number = $request->input('ss_number');
            $worker->profile_picture = $request->input('ss_number').'.'.$extension;
        }
        $worker->save();
        return redirect('worker');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $worker = Worker::find($id);

        $companies_all = Company::orderBy('name', 'asc')->get();

        foreach($companies_all as $company_all)
        {
            $companies [$company_all->id] = $company_all->name;
        }         

        $dispensaries_all = Dispensary::orderBy('name', 'asc')->get();

        foreach($dispensaries_all as $dispensary_all)
        {
            $dispensaries [$dispensary_all->id] = $dispensary_all->name;
        }    

        return View('dashboard.worker.edit', compact('worker', 'companies', 'dispensaries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

  $validator = Validator::make($request->all(),[
                'cnic' => 'numeric|required' , 
                'mobile_number' => 'numeric|required' ,
                'name' => 'required|max:30' , 
                'father_name' => 'required' ,
                'address'  => 'required'
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('worker/'.$id.'/edit')->withInput()->withErrors($validator);   
            }


        if ($request->input('ss_number'))
        {
            // Saving the documents
            //Validate Inputs
            $validator = Validator::make($request->all(),[
                'profile_picture_file' => 'required|mimes:jpg,png,pdf,doc,docx,jpeg|max:5120' , 
                'ss_number' => 'numeric|unique:dependents|unique:workers,ss_number,'.$id
            ]);
            // Check to see if validation fails or passes
            if ($validator->fails())
            {
                return redirect('worker/'.$id.'/edit')->withInput()->withErrors($validator);   
            }
            //saving file to folder
            $file = $request->input('profile_picture_file');
            $fileNameOriginal = pathinfo($file, PATHINFO_FILENAME); // file
            $extension = pathinfo($file, PATHINFO_EXTENSION); // jpg    
            $destinationPath = public_path().'/documents/';
            $extension = Input::file('profile_picture_file')->getClientOriginalExtension(); // getting image extension
            $fileName =  $request->input('ss_number').'.'.$extension; // renameing image
            if (Input::file('profile_picture_file')) 
            {
                 Input::file('profile_picture_file')->move($destinationPath, $fileName);
            }
            else
            {
                echo "no file";
            }

        }

        $worker = Worker::find($id);
        $worker->name = $request->input('name');
        $worker->father_name = $request->input('father_name');
        $worker->cnic = $request->input('cnic');
        $worker->mobile_number = $request->input('mobile_number');
        $worker->gender = $request->input('gender');
        $worker->address = $request->input('address');
        $worker->marital_status = $request->input('marital_status');
        $worker->blocked = 0;
        $worker->company_id = $request->input('company_id');
        $worker->dispensary_id = $request->input('dispensary_id');
        $worker->save();


        if ($request->input('ss_number'))
        {
            $worker->ss_number = $request->input('ss_number');
            $worker->profile_picture = $request->input('ss_number').'.'.$extension;
        }
        $worker->save();
        return redirect('worker');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // To delete a profile picture

    public function remove_profile_image($file_name)
    {
        File::delete(public_path().'/documents/'.$file_name);
        $worker = Worker::where('profile_picture', $file_name)->first();
        $worker->profile_picture = '';
        $worker->save();
    }
}
