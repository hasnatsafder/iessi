<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Account;
use App\Company;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $accounts = Account::all();

        $companies = [];

        foreach ($accounts as $key => $value) {

            $company = Company::find($value->company_id);

            $companies[$key] = $company->name;
        }

        return View('dashboard.accounts.index', compact('accounts', 'companies'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $company_all = Company::all();

        $companies = [];

        foreach ($company_all as $key => $value) {
            $companies[$value->id] = $value->name;
        }

        return View('dashboard.accounts.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account = new Account;

        $account->cheque_number = $request->input('cheque_number');
        $account->company_id = $request->input('company_id');
        $account->amount = $request->input('amount');
        $account->status = $request->input('status');
        $account->date_cheque = $request->input('date_cheque');
        $account->date_deposit = $request->input('date_deposit');
        $account->save();

        return redirect('accounts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
