<?php


Route::resource('company' , 'CompanyController');

Route::resource('dispensary' , 'DispensaryController');
Route::get('dispensary/destroy/{id}', 'DispensaryController@destroy');

Route::resource('worker' , 'WorkerController');
Route::get('worker/deletepicture/{file_name}', 'WorkerController@remove_profile_image');

Route::resource('dependent' , 'DependantController');
Route::get('dependent/deletepicture/{file_name}', 'DependantController@remove_profile_image');

Route::resource('medicine' , 'MedicineRegisterationController');
Route::get('medicine/destroy/{id}', 'MedicineRegisterationController@destroy');

Route::resource('lab' , 'LabTestController');
Route::get('lab/destroy/{id}', 'LabTestController@destroy');

Route::get('contribution/show/{company}/{month}' ,'ContributionController@show');
Route::get('contribution/update/{id}/{value}' ,'ContributionController@update');
Route::Post('contribution/store' ,'ContributionController@store');

Route::get('identification/{ss_number}', 'IdentificationController@show');

Route::resource('medical_billing' , 'MedicalBillingController');
Route::get('medical_billing/add_medicine/{medical_billing}/{medicine}/{quantity}', 'MedicalBillingController@add_medicine');
Route::get('medical_billing/add_lab/{medical_billing}/{lab}','MedicalBillingController@add_lab');

Route::resource('accounts' , 'AccountController');

Route::get('/', function () {
    return redirect('company');
});
