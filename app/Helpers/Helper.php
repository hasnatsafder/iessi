<?php // Code within app\Helpers\Helper.php

use App\Documents;
use App\User;
use App\Vehicles;


	function partnerAvailableCheck($id)
	{
		//Profile Image check
        $document['profileimage'] = Documents::where(['driver_id' => $id, 'document_type' => 5])->first();
        if (!$document['profileimage']) {
                return false;
        }

        //License Image Check
        $document['licenseimage'] = Documents::where(['driver_id' => $id, 'document_type' => 4])->first();
        if (!$document['licenseimage']) {
                return false;
        }

        //NIC Image Check
        $document['nicimage'] = Documents::where(['driver_id' => $id, 'document_type' => 2])->first();
        if (!$document['nicimage']) {
                return false;
        } 

       	//Vehicle available
        $vehicles = vehicles::where(['driver_id' => $id, 'state' => 1])->count();
        if ($vehicles === 0) {
                return false;
        }

        // $user = User::find($id);
        // if ($user->state != 1) {
        // 	return false;

        // }

        return true;
	}
