<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispensary extends Model
{
   protected $fillable = ['name', 'contact_number', 'address'];
}
