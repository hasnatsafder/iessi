<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalBilling extends Model
{
    protected $fillable = ['patient_id'];
}
