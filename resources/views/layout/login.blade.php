<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Scooty Auto Service Lahore Pakistan</title>

<!--
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href=" {{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/simple-line-icons/simple-line-icons.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/uniform/css/uniform.default.css') }} ">

    
<link href="{{ asset('public/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/pages/css/login-soft.css') }}" rel="stylesheet" type="text/css"/>

    
<link rel="stylesheet" href=" {{ asset('public/css/components.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/plugins.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/layout.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/themes/darkblue.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/themes/custom.css') }} ">

    
<link rel="shortcut icon" href="favicon.ico"/>
-->
   
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }} ">
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
 	<script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/scripts/scooty.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href=" {{ asset('public/css/style.css') }} ">
    
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href=" {{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/simple-line-icons/simple-line-icons.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/uniform/css/uniform.default.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }} ">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link rel="stylesheet" href=" {{ asset('public/css/components-rounded.css') }} ">
<!--<link rel="stylesheet" href=" {{ asset('public/css/plugins.css') }} ">-->
<link rel="stylesheet" href=" {{ asset('public/css/layout.css') }} ">

<!-- END THEME STYLES -->
</head>

<body>
    <div class="hero-image">
        <div class="container">
            <div class="row">
<!--
                <div class="col-md-6 col-xs-12">
                    <div class="logo">
                        <img src="{{ asset('public/img/logo.png') }}" alt="Scooty Logo">
                    </div>

                </div>
-->
                <div class="col-md-12 col-xs-12">
                    <div class="social">
                        <ul class="un-styled">
                            <li>
                                <a href=""><img src="{{ asset('public/img/google-plus.png') }}" alt="Google Plus"></a>
                            </li>
                            <li>
                                <a href=""><img src="{{ asset('public/img/twitter.png') }}" alt="Twitter"></a>
                            </li>
                            <li>
                                <a href=""><img src="{{ asset('public/img/facebook.png') }}" alt=""></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     <div class="signup-logo">
                       <a href="/scooty"> <img src="{{ asset('public/img/logo1.png') }}" alt="Scooty Logo"> </a>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="signup text-center">
<!--
                       <div class="login-box"><p>Login to <strong>Scooty!</strong></p>
                        <a href="{{url('auth/customerlogin')}}" class="btn-default btn-block btn-scooty">Login as Customer</a>
                        <a href="{{url('auth/driverlogin')}}" class="btn-default btn-block btn-scooty btn-yellow">Login as Driver</a>
						</div>                      
-->
                   @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
  
        <div class="container">
            <div class="row">
<div class="col-md-12">
    <p class="copyright">
        Copyright © 2015 Scooty!
    </p>
</div>            </div>
        </div>
   
</body>

</html>