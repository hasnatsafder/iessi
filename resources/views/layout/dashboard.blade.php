<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>IESSI | Dashboard </title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   <!-- BEGIN GLOBAL MANDATORY STYLES -->          
   <link href="{{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
   <link href="{{ asset('public/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ asset('public/plugins/fullcalendar/fullcalendar/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css"/>
   <!-- END PAGE LEVEL PLUGIN STYLES -->
   <!-- BEGIN THEME STYLES --> 
   <link href="{{ asset('public/css/style-metronic.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/css/style-responsive.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/css/pages/tasks.css') }}" rel="stylesheet" type="text/css"/>
   <link href="{{ asset('public/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet" type="text/css"/>
   <!-- END THEME STYLES -->
   <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
   <!-- BEGIN HEADER -->   
   <div class="header navbar navbar-inverse navbar-fixed-top">
      <!-- BEGIN TOP NAVIGATION BAR -->
      <div class="header-inner">
         <!-- BEGIN LOGO -->  
         <a class="navbar-brand" href="index.html">
         <img src="" alt="" class="" />
         </a>
         <!-- END LOGO -->
         <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
         <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
         <img src="assets/img/menu-toggler.png" alt="" />
         </a> 
         <!-- END RESPONSIVE MENU TOGGLER -->
         <!-- BEGIN TOP NAVIGATION MENU -->
         <ul class="nav navbar-nav pull-right">
            <!-- BEGIN NOTIFICATION DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
               <span class="username">Moiz Ahmed</span>
               <i class="icon-angle-down"></i>
               </a>
               <ul class="dropdown-menu">
                  <li><a href="extra_profile.html"><i class="icon-user"></i> My Profile</a>
                  </li>
                  <li class="divider"></li>
                  <li><a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i> Full Screen</a>
                  </li>
                  <li><a href="extra_lock.html"><i class="icon-lock"></i> Lock Screen</a>
                  </li>
                  <li><a href="login.html"><i class="icon-key"></i> Log Out</a>
                  </li>
               </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
         </ul>
         <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   <div class="clearfix"></div>
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <div class="page-sidebar navbar-collapse collapse">
         <!-- BEGIN SIDEBAR MENU -->        
         <ul class="page-sidebar-menu">
            <li class="">
               <a href="{{url('company')}}">
               <i class="icon-home"></i> 
               <span class="title">Company Registration</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="{{url('worker')}}">
               <i class="icon-gift"></i> 
               <span class="title">Worker Registration</span>
               </a>
            </li>
            <li class="">
               <a href="{{url('dependent')}}">
               <i class="icon-bar-chart"></i> 
               <span class="title">Dependant Registration</span>
               </a>
            </li>
            <li class="">
               <a href="{{url('contribution/show/null/null')}}">
               <i class="icon-cogs"></i> 
               <span class="title">Contribution</span>
               <span class="selected "></span>
               </a>
            </li>
            <li class="disabled">
               <a href="cont_check.html">
               <i class="icon-file-text"></i> 
               <span class="title">Contribution Check</span>
               <span class=""></span>
               </a>
            </li>
            <li class="disabled">
               <a href="worker_card.html">
               <i class="icon-table"></i> 
               <span class="title">Worker Card</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="{{url('identification/0')}}">
               <i class="icon-sitemap"></i> 
               <span class="title">Patient Identification</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="{{url('medical_billing')}}">
               <i class="icon-gift"></i> 
               <span class="title">Medical Billing</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="{{url('medicine')}}">
               <i class="icon-leaf"></i> 
               <span class="title">Medicine Registration</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="{{'lab'}}">
               <i class="icon-folder-open"></i> 
               <span class="title">Lab Test Registration</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="{{url('dispensary')}}">
               <i class="icon-bookmark-empty"></i> 
               <span class="title">Dispensary Registration</span>
               <span class=" "></span>
               </a>
            </li>
            <li class="start">
               <a href="{{url('accounts')}}">
               <i class="icon-th"></i> 
               <span class="title">Accounts Section</span>
               <span class="selected"></span>
               </a>
            </li>
            <li class="disabled">
               <a href="user.html">
               <i class="icon-user"></i> 
               <span class="title">User Management</span>
               <span class=""></span>
               </a>
            </li>
            <li class="disabled">
               <a href="reports.html">
               <i class="icon-map-marker"></i> 
               <span class="title">Reports</span>
               <span class=""></span>
               </a>
            </li>
         </ul>
        
         <!-- END SIDEBAR MENU -->
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->               
         <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                     <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                     Widget settings form goes here
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn blue">Save changes</button>
                     <button type="button" class="btn default" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <!-- END BEGIN STYLE CUSTOMIZER -->  
         <!-- BEGIN PAGE HEADER-->
         <div class="row">
            <div class="col-md-12">
               <!-- BEGIN PAGE TITLE & BREADCRUMB-->
               <h3 class="page-title">
                 <!-- Dashboard <small>statistics and more</small> -->
               </h3>
            </div>
        </div>
         <!-- END PAGE HEADER-->

	     <div class="tab-content">
	     	<div class="tab-pane active" id="tab_0">
	     		@yield('content')
	        </div>
	     </div>



   <!-- BEGIN FOOTER -->
   <div class="footer">
      <div class="footer-inner">
         2016 &copy; IESSI - All Rights Reserved.
      </div>
<!--      <div class="footer-tools">
         <span class="go-top">
         <i class="icon-angle-up"></i>
         </span>
      </div>
-->   </div>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="assets/plugins/respond.min.js"></script>
   <script src="assets/plugins/excanvas.min.js"></script> 
   <![endif]-->   
   <script src="{{ asset('public/plugins/jquery-1.10.2.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/jquery-migrate-1.2.1.min.js') }}" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="{{ asset('public/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') }}" type="text/javascript" ></script>
   <script src="{{ asset('public/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>  
   <script src="{{ asset('public/plugins/jquery.cookie.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->

   <script src="{{ asset('public/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>     
   <script src="{{ asset('public/plugins/gritter/js/jquery.gritter.js') }}" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="{{ asset('public/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}" type="text/javascript"></script>
   <script src="{{ asset('public/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>  
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ asset('public/scripts/app.js') }}" type="text/javascript"></script>
   
   <script src="{{ asset('public/scripts/tasks.js') }}" type="text/javascript"></script>        
   <!-- END PAGE LEVEL SCRIPTS -->  
   <script>
      jQuery(document).ready(function() {    
         App.init(); // initlayout and core plugins
   

      });
   </script>
   <script>
   $(document).ready(function() {
      $("li.disabled").css('background-color', '#111');
   $("li.disabled a").click(function() {
     return false;
   });
});
</script> 
   <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>