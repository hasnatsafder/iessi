<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Scooty</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href=" {{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/simple-line-icons/simple-line-icons.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/uniform/css/uniform.default.css') }} ">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('public/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/pages/css/login-soft.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link rel="stylesheet" href=" {{ asset('public/css/components.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/plugins.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/layout.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/themes/darkblue.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/themes/custom.css') }} ">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="">
	<img src="{{ asset('public/img/logo.png') }}" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	@yield('content')
</div>

<div class="copyright">
	 2015 &copy; Scooty.
</div>

<script src="{{ asset('public/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('public/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('public/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/pages/scripts/login-soft.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
  Login.init();{{ asset('public/pages/media/bg/1.jpg') }}
  Demo.init();
       // init background slide images
//       $.backstretch([
//        "{{ asset('public/pages/media/bg/1.jpg') }}",
//        "{{ asset('public/pages/media/bg/2.jpg') }}",
//        "{{ asset('public/pages/media/bg/3.jpg') }}",
//        "{{ asset('public/pages/media/bg/4.jpg') }}"
//        ], {
//          fade: 1000,
//          duration: 8000
//    }
//    );
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>