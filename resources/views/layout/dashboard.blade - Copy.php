<!DOCTYPE html>

<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Scooty</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href=" {{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/simple-line-icons/simple-line-icons.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/uniform/css/uniform.default.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }} ">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link rel="stylesheet" href=" {{ asset('public/css/components-rounded.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/plugins.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/layout.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/themes/default.css') }} ">
<link rel="stylesheet" href=" {{ asset('public/css/themes/custom.css') }} ">
<!-- END THEME STYLES -->

{{-- JQuery at the Top --}}
<script src="{{ asset('public/plugins/jquery.min.js') }}" type="text/javascript"></script>
{{-- End JQuery at the Top --}}

<link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="page-header-fixed page-quick-sidebar-over-content ">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="{{url('dashboard')}}">
			<img src="{{ asset('public/img/logo-big.png') }}" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">		
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="username username-hide-on-mobile">
					Admin </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="{{url('auth/logout')}}">
							<i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="{{url('auth/logout')}}" class="dropdown-toggle">
					<i class="icon-logout"></i>
					</a>
				</li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">

		<div class="page-sidebar navbar-collapse collapse">

			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				{{-- showing menus for just admin --}}
				<?php $user = Auth::user() ?>
				@if ( $user->service_type == 6 )
				<li class="nav-item">
					<a href="{{url('dashboard/admin/map')}}">
					<i class="icon-pointer"></i>
					<span class="title">Map Display</span>
					</a>
				</li>
				<li class="nav-item" >
					<a href="{{url('dashboard/admin/zones')}}">
					<i class="icon-home"></i>
					<span class="title">Manage Zones</span>
					</a>
				</li>
				<li>
					<a href="javascript:;">
					<i class="icon-user"></i>
					<span class="title">User Management</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{url('dashboard/admin/userprofile/showpartner')}}">
							<i class="fa  fa-refresh"></i>
							Manage Partners</a>
						</li>
						<li>
							<a href="{{url('dashboard/admin/userprofile/showcustomer')}}">
							<i class="fa  fa-refresh"></i>
							Manage Customers</a>
						</li>
						<li>
							<a href="{{url('dashboard/admin/userprofile/showvendor')}}">
							<i class="fa  fa-refresh"></i>
							Manage Vendors</a>
						</li>
						<li>
							<a href="{{url('dashboard/admin/userprofile/showadmin')}}">
							<i class="fa  fa-refresh"></i>
							Manage System users</a>
						</li>
					</ul>					
				</li>
				<li>
					<a href="{{url('dashboard/admin/userpayment')}}">
					<i class="fa fa-money"></i>
					<span class="title">Partner Payments</span>
					</a>
				</li>
				<li>
					<a href="{{url('dashboard/admin/rating/driver')}}">
					<i class="icon-bar-chart"></i>
					<span class="title">Ratings</span>
					</a>
				</li>	
				<li>
					<a href="{{url('dashboard/admin/request/show')}}">
					<i class="fa fa-cab"></i>
					<span class="title">Requests</span>
					</a>
				</li>
				<li class="start ">
					<a href="javascript:;">
					<i class="icon-cogs"></i>
					<span class="title">Settings</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{url('dashboard/admin/settings/partnterpayments')}}">
							<i class="fa fa-money"></i>
							Partner Payments</a>
						</li>
						<li>
							<a href="{{url('dashboard/admin/settings/partnterdistance')}}">
							<i class="icon-pointer"></i>
							Parnter Distance</a>
						</li>
					</ul>
				</li>				
				@elseif( $user->service_type == 7 )
				<li>
					<a href="{{url('dashboard/admin/vendor')}}">
					<i class="fa fa-cab"></i>
					<span class="title">Your Partners</span>
					</a>
				</li>	
				@endif
				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-users"></i>
					<span class="title">User Profile</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{url('dashboard/updateprofile')}}">
							<i class="fa  fa-refresh"></i>
							Update Profile</a>
						</li>
						<li>
							<a href="{{url('dashboard/updatepassword')}}">
							<i class="fa fa-repeat"></i>
							Update Password</a>
						</li>
					</ul>
				</li>															
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			 @yield('header')
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a>Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">@yield('header')</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					@yield('content')
				</div>

			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2015 &copy; Scooty
	</div>
</div>

<script src="{{ asset('public/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('public/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="{{ asset('public/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/scripts/demo.js') }}" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {   
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>