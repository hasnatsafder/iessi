@extends('layout.dashboard')

@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){


   function count_total()
   {
       var total = 0;
         $('.contribution-value').each(function(){
             total = total + parseInt($(this).val());
         });
         $('#total').html(total);
   }   

   count_total();

   $('.contribution-value').change(function(){
      count_total();
      var id = $(this).attr('id');
      var value = $(this).val();
      var url = '{{ url() }}'+'/contribution/update/'+ id + '/' + value;
      $.ajax({
        url: url
      });
   });

   $('#search').click(function(){

      var billing_month = $('#billing_month').val();
      var company_id = $('#company_id').val();
      var url = '{{ url() }}'+'/contribution/show/'+ company_id + '/' + billing_month;
      window.location.assign(url);

   });

});
</script>



<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Contributions List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
@include ('errors.list')
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
            <div class="row">
               <!--/span-->
               <div class="col-md-4">
                  <div class="form-group">
                     <label class="control-label">Company</label>
                     {!! Form::select('company_id',$companies,$company,['class' => 'form-control placeholder-no-fix input-box', 'id' => 'company_id']) !!}                     
                  </div>
               </div>                 
               <div class="col-md-4">
                  <div class="form-group">
                     <label class="control-label">Billing Month</label>
                     <input type="text" value="{{$month}}" id="billing_month" name="name" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>  
               <div class="col-md-4">
                  <br>
                  <button type="button" id="search" onclick="submitsearch()" class="btn btn-success">Submit</button>
               </div>           
               <!--/span-->
            </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>SS Number</th>
            <th>Name</th>
            <th>F / H Name</th>
            <th>CNIC</th>
            <th>Contributions</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($contributions as $contribution)
            <tr>
               <td>{{ $contribution->ss_number }}</td>
               <td>{{ $contribution->name }}</td>
               <td>{{ $contribution->father_name }}</td>
               <td>{{ $contribution->cnic }}</td>
               <td><input type="text" value="{{ $contribution->contribution }}" class="form-control contribution-value" id="{{ $contribution->id }}"></td>
            </tr>
         @endforeach
         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> <strong id="total">0</strong></td>
         </tr>
      </tbody>
   </table>
   @if(!$contributions)
   {!!  Form::open( ['method' => 'post', 'url' => 'contribution/store','class'=>'horizontal-form']) !!}
   <input type="hidden" value="{{$company}}" name="company" />
   <input type="hidden" value="{{$month}}" name="month" />
   <input type="submit" class="btn red btn-block" id="create-new" value="Click to Enter Contribution">
   {!! Form::Close() !!}

   @endif
</div>
</div>
@include ('errors.list')
   
@stop    
