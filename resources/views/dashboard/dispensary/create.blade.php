@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Create Dispensary Portfolio</div>
   </div>


   <div class="portlet-body form">
      <!-- BEGIN FORM-->
      {!!  Form::open( ['method' => 'post', 'url' => 'dispensary','class'=>'horizontal-form']) !!}

         <div class="form-body">
            <h3 class="form-section">Dispensary Info</h3>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Dispensary Name</label>
                     <input type="text" id="lastName" name="name" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Contact No.</label>
                     <input type="text" name="contact_number" class="form-control"  placeholder="">
                  </div>
               </div>               
               <!--/span-->
            </div>
            <!--/row-->        
            <h3 class="form-section">Address</h3>
            <div class="row">
               <div class="col-md-12 ">
                  <div class="form-group">
                     <label ></label>
                     <input type="text" name="address" class="form-control" >
                  </div>
               </div>
            </div>
         <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Add</button>
         </div>

      {!! Form::Close() !!}
      <!-- END FORM--> 
   </div>
</div>
@include ('errors.list')
   
@stop    

