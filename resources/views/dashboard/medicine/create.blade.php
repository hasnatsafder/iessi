@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Create Medicine Portfolio</div>
   </div>

@include ('errors.list')
   <div class="portlet-body form">
      <!-- BEGIN FORM-->
      {!!  Form::open( ['method' => 'post', 'url' => 'medicine','class'=>'horizontal-form']) !!}

         <div class="form-body">
            <h3 class="form-section">Medicine Info</h3>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Medicine Name</label>
                     <input type="text" id="lastName" name="name" value="{{old('name')}}" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Price</label>
                     <input type="text" name="price" value="{{old('price')}}" class="form-control"  placeholder="">
                  </div>
               </div>               
               <!--/span-->
            </div>
         <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Add</button>
         </div>

      {!! Form::Close() !!}
      <!-- END FORM--> 
   </div>
</div>
   
@stop    

