@extends('layout.dashboard')

@section('content')

<script type="text/javascript">
   
   var url = "{{ url() }}";

   function destroy(id, name)
   {
      var r = confirm('Are you sure you want to Delete "' + name + '"');
       if (r == true) {
           window.location.assign(url + '/medicine/destroy/' + id);
       } else {
       }
   }

</script>

<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Medince List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
         <div class="btn-group">
            <a href="{{url('medicine/create')}}" class="btn green">Add New <i class="icon-plus"></i></a>
         </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>Reg No.</th>
            <th>Name</th>
            <th>Price</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($medicines as $medicine)
            <tr>
               <td>{{ $medicine->id }}</td>
               <td>{{ $medicine->name }}</td>
               <td>{{ $medicine->price }}</td>
               <td>
                  <a href="{{url('medicine/'.$medicine->id.'/edit')}}">Edit</a> / <a href="javascript:void(0);" onclick="destroy({{$medicine->id}}, '{{$medicine->name}}');">Delete</a>
               </td>
            </tr>
         @endforeach
      </tbody>
   </table>
</div>
</div>
@include ('errors.list')
   
@stop    
