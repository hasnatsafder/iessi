@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Companies List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
         <div class="btn-group">
            <a href="{{url('company/create')}}" class="btn green">Add New <i class="icon-plus"></i></a>
         </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>Reg No.</th>
            <th>Name</th>
            <th>Contact Number</th>
            <th>Address</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($companies as $company)
            <tr>
               <td>{{ $company->id }}</td>
               <td>{{ $company->name }}</td>
               <td>{{ $company->contact_number }}</td>
               <td>{{ $company->address }}</td>
               <td>
                  <a href="{{url('company/'.$company->id.'/edit')}}">Edit</a>
               </td>
            </tr>
         @endforeach
         {!! str_replace('/?', '?', $companies->render()) !!}
      </tbody>
   </table>
</div>
</div>
@include ('errors.list')
   
@stop    

