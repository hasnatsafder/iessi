@extends('layout.dashboard')

@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){

   $('#search').click(function(){

      var ss_number = $('#ss_number').val();
      var url = '{{ url() }}'+'/identification/'+ ss_number;
      window.location.assign(url);

   });

});
</script>



<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Workers List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
            <div class="row">
               <!--/span-->
               <div class="col-md-4">
                  <div class="form-group">
                     <label class="control-label">Workers</label>
                     <input type="text" value="{{$ss_number}}" id="ss_number" class="form-control"/>
                  </div>
               </div>                 
               <div class="col-md-4">
                  <br>
                  <button type="button" id="search" onclick="submitsearch()" class="btn btn-success">Submit</button>
               </div>  
               <div class="col-md-4">
                 
               </div>           
               <!--/span-->
            </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>SS Number</th>
            <th>Name</th>
            <th>F / H Name</th>
            <th>CNIC</th>
            <th>Picture</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($worker as $w)
            <tr>
               <td>{{ $w->ss_number }}</td>
               <td>{{ $w->name }}</td>
               <td>{{ $w->father_name }}</td>
               <td>{{ $w->cnic }}</td>
               <td style="width:150px;height:150"><img src="{{ asset('public/documents/'.$w->profile_picture) }}" width="150px" height="150px"></td>
            </tr>
         @endforeach
      </tbody>
   </table>
</div>

<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
      <h3 class="form-section">Dependents</h3>
      </div>
      <thead class="flip-content">
         <tr>
            <th>SS Number</th>
            <th>Name</th>
            <th>F / H Name</th>
            <th>CNIC</th>
            <th>Picture</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($dependents as $w)
            <tr>
               <td>{{ $w->ss_number }}</td>
               <td>{{ $w->name }}</td>
               <td>{{ $w->father_name }}</td>
               <td>{{ $w->cnic }}</td>
               <td style="width:150px;height:150"><img src="{{ asset('public/documents/'.$w->profile_picture) }}" width="150px" height="150px"></td>
            </tr>
         @endforeach
      </tbody>
   </table>
</div>


</div>
@include ('errors.list')
   
@stop    
