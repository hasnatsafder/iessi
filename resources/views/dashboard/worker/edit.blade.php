@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Update Worker Portfolio for {{$worker->name}}</div>
   </div>

@include ('errors.list')
   <div class="portlet-body form">
      <!-- BEGIN FORM-->
      {!!  Form::model($worker, ['method' => 'put', 'url' => 'worker/'.$worker->id,'class'=>'horizontal-form', 'files' => true]) !!}

         <div class="form-body">
            <h3 class="form-section">Worker Info</h3>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">SS No.</label>
                     <input type="text" name="ss_number" value="{{$worker->ss_number}}" class="form-control"  placeholder="">
                  </div>
               </div>                 
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Name</label>
                     <input type="text" value="{{$worker->name}}" id="lastName" name="name" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>             
               <!--/span-->
            </div>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Father / Husband Name</label>
                     <input type="text" value="{{$worker->father_name}}" id="lastName" name="father_name" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">CNIC</label>
                     <input type="text" value="{{$worker->cnic}}" maxlength="13" name="cnic" class="form-control"  placeholder="3420241923169">
                  </div>
               </div>               
               <!--/span-->
            </div>  
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Mobile Number</label>
                     <input type="text" value="{{$worker->mobile_number}}" id="lastName" maxlength="11" name="mobile_number" class="form-control"  placeholder="03335382422">
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Gender</label>
                     {!! Form::select('gender',['male' => 'Male', 'female' => 'Female'],$worker->gender,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!} 
                  </div>
               </div>               
               <!--/span-->
            </div> 
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Marital Status</label>
                     {!! Form::select('marital_status',['single' => 'Single', 'married' => 'Married', 'widowed' => 'Widowed'],$worker->marital_status,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}                     
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Status</label>
                     {!! Form::select('blocked',['0' => 'Active', '1' => 'Blocked'],$worker->blocked,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}
                  </div>
               </div>               
               <!--/span-->
            </div> 
            <div class="row">
               <!--/span-->
               <div class="col-md-12">
                  <div class="form-group">
                     <label class="control-label">Profile Picture</label>
                     <div id="profile_picture_area">
                        @if($worker->profile_picture)
                        <br>
                        <img src="{{ asset('public/documents/'.$worker->profile_picture) }}" width="75px" height="75px">
                        <button type="button" id="remove-picture" docid="{{$worker->profile_picture}}" class="btn btn-danger">Remove</button>
                        @else
                        <input type="file" id="profile_picture_file" name="profile_picture_file" class="form-control" placeholder="">
                        @endif
                     </div>
                     <span class="help-block"></span>
                  </div>
               </div>               
               <!--/span-->
            </div>                                                             
            <!--/row-->        
            <h3 class="form-section">Address</h3>
            <div class="row">
               <div class="col-md-12 ">
                  <div class="form-group">
                     <label ></label>
                     <input type="text" value="{{$worker->address}}" name="address" class="form-control" >
                  </div>
               </div>
            </div>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Company</label>
                        {!! Form::select('company_id',$companies,$worker->company_id,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}                     
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Dispensaries</label>
                     {!! Form::select('dispensary_id',$dispensaries,$worker->dispensary_id,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}                     
                  </div>
               </div>               
               <!--/span-->
            </div>             
         <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Add</button>
         </div>

      {!! Form::Close() !!}
      <!-- END FORM--> 
   </div>
</div>
   
@stop    

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){

   $('#remove-picture').click(function(){
      removeDocument($(this).attr('docid'));
      $('#profile_picture_area').html('<input name="profile_picture_file" type="file" class="form-control">');
   });

   function removeDocument(id)
   {
      var deleteUrl = "{{ url() }}" + "/worker/deletepicture/"+id ;
      $.post(deleteUrl, {"_method" : "Get"}, function(response) {
           
        console.log(response);
        });
   }     
});
</script>


