@extends('layout.dashboard')

@section('content')


<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Worker List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
         <div class="btn-group">
            <a href="{{url('worker/create')}}" class="btn green">Add New <i class="icon-plus"></i></a>
         </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>Reg No.</th>
            <th>SS Number</th>
            <th>Name</th>
            <th>F / H Name</th>
            <th>CNIC</th>
            <th>Status</th>
            <th>Company</th>
            <th>Dispensary</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($workers as $worker)
            <tr>
               <td>{{ $worker->id }}</td>
               <td>{{ $worker->ss_number }}</td>
               <td>{{ $worker->name }}</td>
               <td>{{ $worker->father_name }}</td>
               <td>{{ $worker->cnic }}</td>
               <td>
                  @if($worker->blocked == 0)
                     Yes
                  @else
                     No
                  @endif   
               </td>
               <td>{{ $worker->company_name }}</td>
               <td>{{ $worker->dispensary_name }}</td>
               <td>
                  <a href="{{url('worker/'.$worker->id.'/edit')}}">Edit</a>
               </td>
            </tr>
         @endforeach
         {!! str_replace('/?', '?', $workers->render()) !!}
      </tbody>
   </table>
</div>
</div>
@include ('errors.list')
   
@stop    
