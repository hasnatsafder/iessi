@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Create Worker Portfolio</div>
   </div>

@include ('errors.list')
   <div class="portlet-body form">
      <!-- BEGIN FORM-->
      {!!  Form::open( ['method' => 'post', 'url' => 'worker','class'=>'horizontal-form', 'files' => true]) !!}

         <div class="form-body">
            <h3 class="form-section">Worker Info</h3>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">SS No.</label>
                     <input type="text" name="ss_number" value="{{old('ss_number')}}" class="form-control"  placeholder="">
                  </div>
               </div>                 
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Name</label>
                     <input type="text" value="{{old('name')}}" id="lastName" name="name" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>             
               <!--/span-->
            </div>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Father / Husband Name</label>
                     <input type="text" value="{{old('father_name')}}" id="lastName" name="father_name" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">CNIC</label>
                     <input type="text" value="{{old('cnic')}}" maxlength="13" name="cnic" class="form-control"  placeholder="3420241923169">
                  </div>
               </div>               
               <!--/span-->
            </div>  
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Mobile Number</label>
                     <input type="text" value="{{old('mobile_number')}}" id="lastName" maxlength="11" name="mobile_number" class="form-control"  placeholder="03335382422">
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Gender</label>
                     {!! Form::select('gender',['male' => 'Male', 'female' => 'Female'],old('gender'),['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!} 
                  </div>
               </div>               
               <!--/span-->
            </div> 
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Marital Status</label>
                     {!! Form::select('marital_status',['single' => 'Single', 'married' => 'Married', 'widowed' => 'Widowed'],old('marital_status'),['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}                     
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Status</label>
                     {!! Form::select('blocked',['0' => 'Active', '1' => 'Blocked'],old('blocked'),['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}
                  </div>
               </div>               
               <!--/span-->
            </div> 
            <div class="row">
               <!--/span-->
               <div class="col-md-12">
                  <div class="form-group">
                     <label class="control-label">Profile Picture</label>
                     <input type="file" id="lastName" name="profile_picture_file" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>               
               <!--/span-->
            </div>                                                             
            <!--/row-->        
            <h3 class="form-section">Address</h3>
            <div class="row">
               <div class="col-md-12 ">
                  <div class="form-group">
                     <label ></label>
                     <input type="text" value="{{old('address')}}" name="address" class="form-control" >
                  </div>
               </div>
            </div>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Company</label>
                        {!! Form::select('company_id',$companies,old('company_id'),['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}                     
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Dispensaries</label>
                     {!! Form::select('dispensary_id',$dispensaries,old('dispensary_id'),['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!}                     
                  </div>
               </div>               
               <!--/span-->
            </div>             
         <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Add</button>
         </div>

      {!! Form::Close() !!}
      <!-- END FORM--> 
   </div>
</div>
   
@stop    

