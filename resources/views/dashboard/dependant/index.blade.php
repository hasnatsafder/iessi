@extends('layout.dashboard')

@section('content')


<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Dependents List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
         <div class="btn-group">
            <a href="{{url('dependent/create')}}" class="btn green">Add New <i class="icon-plus"></i></a>
         </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>Reg No.</th>
            <th>SS Number</th>
            <th>Name</th>
            <th>F / H Name</th>
            <th>CNIC</th>
            <th>Status</th>
            <th>Relation</th>
            <th>Dispensary</th>
            <th>Worker</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($dependents as $dependent)
            <tr>
               <td>{{ $dependent->id }}</td>
               <td>{{ $dependent->ss_number }}</td>
               <td>{{ $dependent->name }}</td>
               <td>{{ $dependent->father_name }}</td>
               <td>{{ $dependent->cnic }}</td>
               <td>
                  @if($dependent->blocked == 0)
                     Yes
                  @else
                     No
                  @endif   
               </td>
               <td>{{ $dependent->relation }}</td>
               <td>{{ $dependent->dispensary_name }}</td>
               <td>{{ $dependent->worker_name }}</td>
               <td>
                  <a href="{{url('dependent/'.$dependent->id.'/edit')}}">Edit</a>
               </td>
            </tr>
         @endforeach
      </tbody>
   </table>
</div>
</div>
@include ('errors.list')
   
@stop    
