@extends('layout.dashboard')

@section('content')

<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Accounts List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
         <div class="btn-group">
            <a href="{{url('accounts/create')}}" class="btn green">Add New <i class="icon-plus"></i></a>
         </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>Reg No.</th>
            <th>Cheque No.</th>
            <th>Company</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Date on Cheque</th>
            <th>Date of Deposit</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($accounts as $key => $account)
            <tr>
               <td>{{ $account->id }}</td>
               <td>{{ $account->cheque_number }}</td>
               <td>{{ $companies[$key] }}</td>
               <td>{{ $account->amount }}</td>
               <td>{{ $account->status }}</td>
               <td>{{ $account->date_cheque }}</td>
               <td>{{ $account->date_deposit }}</td>
            </tr>
         @endforeach
      </tbody>
   </table>
</div>
</div>
@include ('errors.list')
   
@stop    
