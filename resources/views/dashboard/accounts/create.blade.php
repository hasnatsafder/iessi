@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Accounts Section</div>
      <div class="tools">
         <a href="javascript:;" class="collapse"></a>
         <a href="javascript:;" class="reload"></a>
      </div>
   </div>


   <div class="portlet-body form">
      <!-- BEGIN FORM-->
      {!!  Form::open( ['method' => 'post', 'url' => 'accounts','class'=>'horizontal-form']) !!}
         <div class="form-body">
            <h3 class="form-section">Contribution Cheque Info</h3>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Cheque No.</label>
                     <input type="text" name="cheque_number" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Company Name</label>
                     {!! Form::select('company_id',$companies,null,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!} 
                     <span class="help-block"></span>
                  </div>
               </div>
               <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Amount</label>
                     <input type="text" name="amount" class="form-control" placeholder="">
                     <span class="help-block"></span>
                  </div>
               </div>
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Status</label>
                     <select  class="form-control" name="status">
                        <option value="cleared">Cleared</option>
                        <option value="bounced">Bounced</option>
                     </select>
                     <span class="help-block"></span>
                  </div>
               </div>
            </div>
            <!--/row-->
            <div class="row">
            <div class="col-md-6">
               <label class="control-label">Date on Cheque</label>
                  <div class="">
                  <div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                  <input type="text" class="form-control dp" name="date_cheque"> 
               </span>
            </div>           
            </div>                      
         <!-- /input-group -->      
      </div>
            <div class="col-md-6">
               <label class="control-label">Date of Deposit</label>
                  <div class="">
                  <div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                  <input type="text" class="form-control dp" name="date_deposit">
               </span>
            </div>           
         <!-- /input-group -->      
      </div>

   </div>

               <!--/span-->
            </div>
               <!--/span-->
            </div>
            <!--/row-->        
         <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
         </div>
      {!! Form::Close() !!} 
      <!-- END FORM--> 
   </div>

</div>
@include ('errors.list')
   
@stop    

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){
   $('.dp').datepicker({ 
     dateFormat: 'yy-mm-dd',
     constrainInput: false
   });
});
</script>