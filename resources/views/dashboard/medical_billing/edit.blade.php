@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Create Medical Billing</div>
   </div>

   <input type="hidden" id="medical_billing" value="{{$id}}">
   <div class="portlet-body form">
      <!-- BEGIN FORM-->
         <div class="form-body">
         <h3 class="form-section">Medicines</h3>

         <table class="table table-bordered table-striped table-condensed flip-content">
               <thead class="flip-content">
                  <tr>
                     <th>Medicine</th>
                     <th>Quantity</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach ($medicines_medical_billing as $key => $medical_billing)
                     <tr>
                        <td>{{ $medical_billing->medicine }}</td>
                        <td>{{ $medical_billing->quantity }}</td>
                     </tr>
                  @endforeach
               </tbody>
            </table>         
            <div class="row">
               <!--/span-->
               <div class="col-md-12">
                  <div class="form-group" id="medicine-div">
                     <span>
                        {!! Form::select('dispensary_id',$medicines,null,['id' => 'medicine_id','class' => 'placeholder-no-fix input-box', 'placeholder' => 'State']) !!} 
                     </span>
                     <span><input id="medicine_quantity"/></span>
                     <span><button class="btn btn-success" id="add_medicine">Add</button></span>
                     <span class="help-block"></span>
                  </div>
               </div>
            </div>
            <h3 class="form-section">Labs</h3>

            <table class="table table-bordered table-striped table-condensed flip-content">
               <thead class="flip-content">
                  <tr>
                     <th>Lab</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach ($lab_medical_billing as $key => $medical_billing)
                     <tr>
                        <td>{{ $medical_billing->lab }}</td>
                     </tr>
                  @endforeach
               </tbody>
            </table>      

            <div class="row">     
               <div class="col-md-12">
                  <div class="form-group" id="lab-div">
                     <span>
                        {!! Form::select('dispensary_id',$lab_tests,null,['id' => 'lab_id','class' => 'placeholder-no-fix input-box', 'placeholder' => 'State']) !!} 
                     </span>
                     <span><button class="btn btn-success" id="add_lab">Add</button></span>
                     <span class="help-block"></span>
                  </div>
               </div>                            
               <!--/span-->
            </div>
      <!-- END FORM--> 
   </div>
</div>
@include ('errors.list')
   
@stop    

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){

   $('#add_medicine').click(function(){
      var medicine_id = $('#medicine_id').val();
      var medicine_quantity = $('#medicine_quantity').val();
      var medical_billing = $('#medical_billing').val();
      addMedicine(medicine_id, medicine_quantity, medical_billing);
   });

   function addMedicine(medicine_id, medicine_quantity, medical_billing)
   {
      var insertUrl = "{{ url() }}" + "/medical_billing/add_medicine/"+medical_billing+"/"+medicine_id+"/"+medicine_quantity ;
      $.get(insertUrl, function(data, status){
              alert(data);
              location.reload();
          });
   }   

   //adding lab
   $('#add_lab').click(function(){
      var lab_id = $('#lab_id').val();
      var medical_billing = $('#medical_billing').val();
      addLab(lab_id, medical_billing);
   });

   function addLab(lab_id, medical_billing)
   {
      var insertUrl = "{{ url() }}" + "/medical_billing/add_lab/"+medical_billing+"/"+lab_id ;
      $.get(insertUrl, function(data, status){
              alert(data);
              location.reload();
          });
   }  
});
</script>
