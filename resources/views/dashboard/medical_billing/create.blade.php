@extends('layout.dashboard')

@section('content')
<div class="portlet box green">
   <div class="portlet-title">
      <div class="caption"><i class="icon-reorder"></i>Create Medical Billing</div>
   </div>


   <div class="portlet-body form">
      <!-- BEGIN FORM-->
      {!!  Form::open( ['method' => 'post', 'url' => 'medical_billing','class'=>'horizontal-form']) !!}

         <div class="form-body">
            <h3 class="form-section">Medical Billing Info</h3>
            <div class="row">
               <!--/span-->
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Dispensary Name</label>
                     {!! Form::select('dispensary_id',$dispensaries,null,['class' => 'form-control placeholder-no-fix input-box', 'placeholder' => 'State']) !!} 
                     <span class="help-block"></span>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Patient SS Number.</label>
                     <input type="text" name="patient_id" class="form-control"  placeholder="">
                  </div>
               </div>               
               <!--/span-->
            </div>
         <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Add</button>
         </div>

      {!! Form::Close() !!}
      <!-- END FORM--> 
   </div>
</div>
@include ('errors.list')
   
@stop    

