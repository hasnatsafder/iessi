@extends('layout.dashboard')

@section('content')

<div class="portlet box green">
<div class="portlet-title">
   <div class="caption"><i class="icon-cogs"></i>Medical Billing List</div>
   <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
   </div>
</div>
<div class="portlet-body flip-scroll">
   <table class="table table-bordered table-striped table-condensed flip-content">
      <div class="table-toolbar">
         <div class="btn-group">
            <a href="{{url('medical_billing/create')}}" class="btn green">Add New <i class="icon-plus"></i></a>
         </div>

      </div>
      <thead class="flip-content">
         <tr>
            <th>Reg No.</th>
            <th>Dispensary</th>
            <th>SS Number</th>
            <th>Total</th>
            <th>Medicines and Lab</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($medical_billings as $key => $medical_billing)
            <tr>
               <td>{{ $medical_billing->id }}</td>
               <td>{{ $dispensary[$key] }}</td>
               <td>{{ $medical_billing->patient_id }}</td>
               <td>{{ $total[$key] }}</td>
               <td>
                  <a href="{{url('medical_billing/'.$medical_billing->id.'/edit')}}">Add Medicine/Lab</a>
               </td>
            </tr>
         @endforeach
      </tbody>
   </table>
</div>
</div>
@include ('errors.list')
   
@stop    
